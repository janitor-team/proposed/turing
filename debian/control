Source: turing
Section: x11
Priority: optional
Maintainer: Georges Khaznadar <georgesk@debian.org>
Build-Depends: debhelper-compat (=12), python3-all, dh-python,
 python3-pyqt5, pyqt5-dev-tools, qttools5-dev-tools, qtbase5-dev
Standards-Version: 4.5.0
Homepage: https://turingapp.ml/
Vcs-Git: https://salsa.debian.org/georgesk/turing.git
Vcs-Browser: https://salsa.debian.org/georgesk/turing
X-Python3-Version: >=3.5

Package: turing
Architecture: all
Depends: python3, ${misc:Depends}, ${python3:Depends},
 python3-altgraph, python3-autopep8, python3-cycler, python3-docutils,
 python3-future, python3-jedi, python3-kiwisolver, python3-macholib,
 python3-numpy, python3-parso, python3-pefile, python3-pep8,
 python3-pyflakes, python3-pygments, python3-pyparsing, python3-pyqt5,
 python3-dateutil, python3-tz, python3-matplotlib
Description: assistant to learn algorithms and programming languages
 Turing is a free and cross-platform app whose main goal is to assist
 the learning of algorithms and programming languages by providing
 easy-to-use development tools to all.
 .
 It provides a lighter alternative to the well-known Algobox, which is
 the currently de-facto widely used solution.
 .
 It provides two work modes:
 .
 * Algorithm mode
     * Uses a "natural" pseudocode language similar to the one used in
       Algobox and school books.
     * Assisted development
 * Program mode
     * Uses Python, for the more experienced
 .
 In both modes, the code can be debugged and executed step-by-step to
 facilitate the problem-solving side of development.

 